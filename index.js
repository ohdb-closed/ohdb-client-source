/**
 * index.js - ohdb-client
 * Licensed under GPLv3.
 * Copyright (C) 2015 OHDB.
 **/

(function () {
    "use strict";

    // trust cheap SSL signatures in development
    require('ssl-root-cas').inject();

    var Q = require('q'),
        rc = require('rc'),
        request = require('request'),
        map = require('map-stream'),
        auth = {
            user: null,
            pass: null
        },
        ohdb = {
            // create the base url for all
            // API requests
            api: function (url) {
                var base = 'api.ohdb.ca',

                    // RFC 1738 (basic auth in url)
                    authStr = auth.user + ':' + auth.pass,

                    // within a production enviroment (on Heroku),
                    // we need to use a non-SSL protocol. But when
                    // testing locally, we must use HTTPS.
                    protocol = 'https';

                // fix url first character
                if (url[0] === '/') {
                    url = url.substr(1);
                }

                // construct and return
                return protocol + '://' + authStr + '@' + base + '/' + url;
            },

            // stream-forced request-request with
            // enforced options
            fetch: function (options, callback) {
                var stream;

                // json-only communication is very
                // important
                options.json = true;

                // the api will only allow a
                // proper user agent string to
                // be able to redirect browsers
                options.headers = {
                    'User-Agent': 'X-OHDB-Client'
                };

                // all URLs are appended to the proper url path,
                // and authentication is added
                options.url = ohdb.api(options.url);

                // create a stream with request, and map the
                // callback over it
                stream = request(options, function (err, res) {
                    // if no error, create an error
                    // on bad HTTP response
                    if (!res || res.statusCode >= 400) {
                        err = err || new Error('Something went wrong (status: ' + (res ? res.statusCode : 500) + ')');
                    }

                    // if error exists, throw it
                    if (err) {
                        throw err;
                    }
                }).pipe(map(function (src, fn) {
                    var json;

                    // parse to json forcefully
                    try {
                        json = JSON.parse(src.toString('utf8'));
                    } catch (e) {
                        json = {
                            status: 'error',
                            message: String(e)
                        };
                    }

                    // continue
                    fn(null, json);
                }));

                // if callback is specified, map
                // it over the stream
                if (callback && typeof callback === 'function') {
                    stream.pipe(map(callback));
                }

                // always return the stream
                return stream;
            },

            // allow extending of the get
            // function
            fn: {
                // ask the api for its version,
                // then parse it and send it down
                // the stream
                version: function () {
                    return ohdb.fetch({
                        url: '/version'
                    }).pipe(map(function (res, fn) {
                        fn(null, res.version);
                    }));
                },

                // just fetch the info belonging to
                // the api user
                me: function () {
                    return ohdb.fetch({
                        url: '/me'
                    });
                },

                // send an echo request
                echo: function () {
                    return ohdb.fetch({
                        url: '/echo/post',
                        method: 'POST',
                        body: {
                            echo: Array.prototype.slice.call(arguments)
                        }
                    });
                },

                people: function (params) {
                    return ohdb.fetch({
                        url: '/people',
                        method: 'GET',
                        body: params
                    });
                },

                records: function (params) {
                    return ohdb.fetch({
                        url: '/records',
                        method: 'GET',
                        body: params
                    });
                }
            },

            // simple callback execution
            get: function (key) {
                // grab all arguments except
                // the key
                var args = Array.prototype.slice.call(arguments, 1);

                // execute and return
                return ohdb.fn[key].apply(this, args);
            },

            // connect back to the api,
            // save the credentials (temporarily),
            // and verify by fetching the version
            connect: function (authkey, authsecret) {
                var promise = Q.defer(),
                    postConnect = function (key, secret) {
                        // set global key/secret pair
                        auth.user = key;
                        auth.pass = secret;

                        // do basic request
                        ohdb.fetch({
                            url: '/version'
                        }, function (src, fn) {
                            if (src.version === 'unknown') {
                                promise.reject('error');
                            } else {
                                promise.resolve('success');
                            }

                            fn(null, src);
                        });
                    },
                    config = rc('ohdb', {
                        key: authkey || '',
                        secret: authsecret || ''
                    });

		if (config.key && config.secret) {
			postConnect(config.key, config.secret);
		} else {
			throw new Error('unable to find credentials.');
		}

                return promise.promise;
            }
        };

    // expose
    module.exports = ohdb;
}());
