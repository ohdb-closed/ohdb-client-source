# ohdb-client

The Node.js client for using OHDB's records in research.

## usage

### installation
`npm install --save ohdb`

### getting started

```
var ohdb = require('ohdb');
var client = ohdb.connect('my-api-key');

// .. do stuff with client ..
```

More documentation and examples are available at [OHDB's docs site](http://docs.ohdb.ca/).

## authors

 - Alina Jahani <alina@ohdb.ca>
 - Mahimul Hoque <mahimul@ohdb.ca>
 - Karim Alibhai <karim@ohdb.ca>